Set-PSReadLineKeyHandler -Key UpArrow -Function HistorySearchBackward
Set-PSReadLineKeyHandler -Key DownArrow -Function HistorySearchForward
Set-Alias -Name vi -Value vim
# [Console]::OutputEncoding = [System.Text.Encoding]::UTF8

