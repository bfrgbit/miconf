$INTERNET_TEST_HOST = 'google.com'
$WAIT_INTERVAL_SEC = 110

While (1) {
    $TimeStart = Get-Date
    $PingOutput = Test-Connection -Count 1 $INTERNET_TEST_HOST 2>$Null
    If (( "" -eq $PingOutput ) -or ( $Null -eq $PingOutput )) {
        Write-Output "$(Get-Date) - Internet connection failed"
    }
    Else {
        Write-Output "$(Get-Date) - Starting OpenVPN connection..."
        Start-Process -Wait -NoNewWindow -FilePath 'C:\Program Files\OpenVPN\bin\openvpn.exe' -ArgumentList '--config "C:\Program Files\OpenVPN\config\client.ovpn"'
        Write-Output "$(Get-Date) - Starting OpenVPN connection... Exited!"
    }
    $TimeEnd = Get-Date

    $TimeDiff = $TimeEnd - $TimeStart
    $WaitSec = 10
    If ($TimeDiff.TotalSeconds -lt $WAIT_INTERVAL_SEC) {
        $WaitSec = $WaitSec + [int]($WAIT_INTERVAL_SEC - $TimeDiff.TotalSeconds)
    }
    Write-Output "$(Get-Date) - Waiting for $WaitSec second(s)..."
    Start-Sleep -Seconds $WaitSec
}
