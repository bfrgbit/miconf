export PYENV_ROOT="${HOME}/.pyenv"
export PATH="${PYENV_ROOT}/bin:${PATH}"
echo "$(timestamp) - Init pyenv..."
if which pyenv && eval "$(pyenv init -)"
then
  echo "$(timestamp) - Init pyenv... Done!"
else
  echo "$(timestamp) - Init pyenv... Failed!"
fi

