export NVM_DIR="${HOME}/.nvm"
echo "$(timestamp) - Init nvm..."
nvm_loaded="yes"
[ -s "${NVM_DIR}/nvm.sh" ] && \. "${NVM_DIR}/nvm.sh" || nvm_loaded="no"  # This loads nvm
[ -s "${NVM_DIR}/bash_completion" ] && \. "${NVM_DIR}/bash_completion" || nvm_loaded="no"  # This loads nvm bash_completion
which "${NVM_DIR}/nvm-exec"
if [ "${nvm_loaded}" = "yes" ]
then
  echo "$(timestamp) - Init nvm... Done!"
else
  echo "$(timestamp) - Init nvm... Failed!"
fi

