export RBENV_ROOT="${HOME}/.rbenv"
export PATH="${RBENV_ROOT}/bin:$PATH"
echo "$(timestamp) - Init rbenv..."
if which rbenv && eval "$(rbenv init -)"
then
  echo "$(timestamp) - Init rbenv... Done!"
else
  echo "$(timestamp) - Init rbenv... Failed!"
fi

