export JENV_ROOT="${HOME}/.jenv"
export PATH="${JENV_ROOT}/bin:${PATH}"
echo "$(timestamp) - Init jEnv..."
if which jenv && eval "$(jenv init -)"
then
  echo "$(timestamp) - Init jEnv... Done!"
else
  echo "$(timestamp) - Init jEnv... Failed!"
fi

